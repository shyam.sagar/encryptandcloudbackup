﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace EncryptAndCloudBackup
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// Silent arg(-s) for launching the background backup service. The files to backup are stored in 
        /// "FileLocation" variable that VS stores in settings file that is saved at location exe.
        /// Remove arg(-r) will erase this var's content.
        /// Without args will launch UI.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Gets cmd line args.
            string[] args = Environment.GetCommandLineArgs();

            FileOps fileOps = new FileOps();
            List<String> selectedFolders = new List<String>();

            if (args.Length>1)
            {
                if (args[1] == "-s")
                {
                    //Export all file locations to a data structure.
                    selectedFolders = fileOps.getFolders();
                }

                //TODO: Make this arg bigger so it is not entered by mistake.
                else if (args[1] == "-r")
                {
                    //Remove all locations
                    Properties.Settings.Default["FileLocation"] = "";
                    Properties.Settings.Default.Save();
                }
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
        }


    }
}
