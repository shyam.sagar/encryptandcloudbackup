﻿using System;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.Windows.Forms;
using System.Collections.Generic;

namespace EncryptAndCloudBackup
{
    public partial class Form1 : Form
    {
        private List<String> selectedFolders = new List<String>();

        FileOps fileOps = new FileOps();
        public Form1()
        {
            InitializeComponent();
            InitializeSelectedFolders();
            InitializeBackupSavePath();
        }

        // Initialize the path to save backups.
        private void InitializeBackupSavePath()
        {
            string backupPath = fileOps.getBackupFolder();

            this.backupFolderTextBox.Text = backupPath;
        }

        //Initialize the listview with all the saved folders, check all by default.
        private void InitializeSelectedFolders()
        {
            // Added a column to populate listview with Details view.
            this.savedFoldersListView1.Columns.Add("Folder Paths", -2, HorizontalAlignment.Left);

            // Get list of folders from saved data on app run.
            if (selectedFolders.Count == 0)
                selectedFolders = fileOps.getFolders();


            int index = 0;
            foreach(string folderPath in selectedFolders)
            {
                //Add folder path to listview.
                this.savedFoldersListView1.Items.Add(folderPath);

                //Check the path on listview.
                this.savedFoldersListView1.Items[index].Checked = true;
                index++;
            }
        }

        //Code for when addFolder button is pressed.
        //Saves all added directories to "FileLocation" var to retain after closing app.
        //TODO: Implement prevention of file duplication.
        private void addFolderButton_Click(object sender, EventArgs e)
        {
            //Open file dialog to select folders.
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.InitialDirectory = "C:";
            dialog.IsFolderPicker = true;
            dialog.Multiselect = true;

            //When folders are selected and Ok is pressed on the dialog, the control comes here.
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                //Save all locations to a string and save to "FileLocation" variable to retain value 
                //after closing app.
                foreach (string s in dialog.FileNames)
                {
                    // Check if newly added folder already exists or is part of added folder and warn user.
                    this.CheckFolderPathAdd(s);

                    this.selectedFolders.Add(s);

                    //Add folder path to listview.
                    this.savedFoldersListView1.Items.Add(s);

                    //Check the path on listview.
                    this.savedFoldersListView1.Items[this.selectedFolders.Count - 1].Checked = true;
                }

                fileOps.setFolders(this.selectedFolders);
            }
        }



        // Check if newly added folder already exists or is part of added folder and warn user.
        private void CheckFolderPathAdd(string newFolderPath)
        {
            foreach (string existingFolderPath in this.selectedFolders)
            {
                // If new folder contains already added folder.
                if (existingFolderPath.Contains(newFolderPath))
                    this.RemoveDuplicateDialog(newFolderPath, existingFolderPath);

                // If already added folder contains new folder.
                else if (newFolderPath.Contains(existingFolderPath))
                    this.RemoveDuplicateDialog(existingFolderPath, newFolderPath);
            }
        }

        // Show dialod to user if new folder or directory is already added.
        private void RemoveDuplicateDialog(string existingFolderPath, string newFolderPath)
        {
            DialogResult res = MessageBox.Show("The folder \"" + existingFolderPath + "\" already seems to contain \"" + newFolderPath + "\".\nPlease check to avoid duplication.", 
                "Confirmation", 
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        //Change path of backup folder.
        private void changeBackupFolderBtn_Click(object sender, EventArgs e)
        {
            //Open file dialog to select folders.
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.InitialDirectory = "C:";
            dialog.IsFolderPicker = true;

            //When folders are selected and Ok is pressed on the dialog, the control comes here.
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                this.backupFolderTextBox.Text = dialog.FileName;

                fileOps.setBackupFolders(dialog.FileName);
            }

        }


        private void savedFoldersListView1_SelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if(e.IsSelected)
            {
                // Check if newly added folder already exists or is part of added folder and warn user.
                this.CheckFolderPathAdd(e.Item.Text);

                // Add the unselected item to array.
                this.selectedFolders.Add(e.Item.Text);

                // Add item to saved data.
                fileOps.setFolders(this.selectedFolders);
            }

            else
            {
                // Remove the unselected item from array.
                this.selectedFolders.Remove(e.Item.Text);

                // Remove item from saved data and save.
                fileOps.setFolders(this.selectedFolders);
            }
        }

        //TODO: Create save button. Fetch checked paths only.
    }
}
