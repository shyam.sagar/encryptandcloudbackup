﻿namespace EncryptAndCloudBackup
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addFolderButton = new System.Windows.Forms.Button();
            this.savedFoldersListView1 = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.backupFolderTextBox = new System.Windows.Forms.TextBox();
            this.changeBackupFolderBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.confirmPasswordTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // addFolderButton
            // 
            this.addFolderButton.Location = new System.Drawing.Point(353, 292);
            this.addFolderButton.Name = "addFolderButton";
            this.addFolderButton.Size = new System.Drawing.Size(107, 23);
            this.addFolderButton.TabIndex = 0;
            this.addFolderButton.Text = "Add Folder";
            this.addFolderButton.UseVisualStyleBackColor = true;
            this.addFolderButton.Click += new System.EventHandler(this.addFolderButton_Click);
            // 
            // savedFoldersListView1
            // 
            this.savedFoldersListView1.CheckBoxes = true;
            this.savedFoldersListView1.FullRowSelect = true;
            this.savedFoldersListView1.GridLines = true;
            this.savedFoldersListView1.HideSelection = false;
            this.savedFoldersListView1.Location = new System.Drawing.Point(42, 37);
            this.savedFoldersListView1.Name = "savedFoldersListView1";
            this.savedFoldersListView1.Size = new System.Drawing.Size(418, 237);
            this.savedFoldersListView1.TabIndex = 1;
            this.savedFoldersListView1.UseCompatibleStateImageBehavior = false;
            this.savedFoldersListView1.View = System.Windows.Forms.View.Details;
            this.savedFoldersListView1.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(savedFoldersListView1_SelectionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 356);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Backup Path";
            // 
            // backupFolderTextBox
            // 
            this.backupFolderTextBox.Location = new System.Drawing.Point(133, 353);
            this.backupFolderTextBox.Name = "backupFolderTextBox";
            this.backupFolderTextBox.Size = new System.Drawing.Size(327, 22);
            this.backupFolderTextBox.TabIndex = 3;
            // 
            // changeBackupFolderBtn
            // 
            this.changeBackupFolderBtn.Location = new System.Drawing.Point(305, 392);
            this.changeBackupFolderBtn.Name = "changeBackupFolderBtn";
            this.changeBackupFolderBtn.Size = new System.Drawing.Size(155, 26);
            this.changeBackupFolderBtn.TabIndex = 4;
            this.changeBackupFolderBtn.Text = "Change backup path";
            this.changeBackupFolderBtn.UseVisualStyleBackColor = true;
            this.changeBackupFolderBtn.Click += new System.EventHandler(this.changeBackupFolderBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(613, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Password (Optional)";
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(751, 34);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.PasswordChar = '*';
            this.passwordTextBox.Size = new System.Drawing.Size(220, 22);
            this.passwordTextBox.TabIndex = 10;
            this.passwordTextBox.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(613, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Confirm Password";
            // 
            // confirmPasswordTextBox
            // 
            this.confirmPasswordTextBox.Location = new System.Drawing.Point(751, 79);
            this.confirmPasswordTextBox.Name = "confirmPasswordTextBox";
            this.confirmPasswordTextBox.PasswordChar = '*';
            this.confirmPasswordTextBox.Size = new System.Drawing.Size(220, 22);
            this.confirmPasswordTextBox.TabIndex = 12;
            this.confirmPasswordTextBox.UseSystemPasswordChar = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 450);
            this.Controls.Add(this.confirmPasswordTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.changeBackupFolderBtn);
            this.Controls.Add(this.backupFolderTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.savedFoldersListView1);
            this.Controls.Add(this.addFolderButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button addFolderButton;
        private System.Windows.Forms.ListView savedFoldersListView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox backupFolderTextBox;
        private System.Windows.Forms.Button changeBackupFolderBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox confirmPasswordTextBox;
    }
}

