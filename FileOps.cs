﻿using System;
using System.Collections.Generic;
using System.IO;

namespace EncryptAndCloudBackup
{
    class FileOps
    {
        //Export all file locations to a data structure.
        List<String> selectedFolders = new List<String>();
        internal List<String> getFolders()
        {
            //Get the string with all folder paths.
            string s = Properties.Settings.Default["FileLocation"].ToString();

            //Store all paths to StringCollection, seperated by ';' delimiter.
            //If ';' is detected, substring from j till j-i characters will be added to StringCollection.
            //j is updated manually after each add op.
            for (int i = 0, j = 0; s.Length > i; i++)
            {
                if (s[i] == ';')
                {
                    selectedFolders.Add(s.Substring(j, i - j));

                    //TODO: Remove after debug.
                    System.IO.File.AppendAllText(@"D:\User\Documents\C#\EncryptAndCloudBackup\bin\Debug\logs.txt",
                    s.Substring(j, i - j) + "\n");

                    j = i + 1;
                }
            }
            selectedFolders.Sort();
            return selectedFolders;
        }

        //Get backup folder path saved by user or default location.
        internal string getBackupFolder()
        {
            //Try to get user saved backup location.
            string backupFolder = Properties.Settings.Default["BackupLocation"].ToString();

            //If vaiable is not set, means it is first run and so default backup folder
            //is set to my documents.
            if (backupFolder.Length == 0)
            {
                //Get MyDocuments path
                var myDocPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                var folderName = "Encrypted_Backup";
                backupFolder = Path.Combine(myDocPath, folderName);
            }

            return backupFolder;
        }

        internal void setFolders(List<String> selectedFolders)
        {
            string saveFolders = "";
            foreach (string s in selectedFolders)
            {
                //System.IO.File.AppendAllText(@"D:\User\Documents\C#\EncryptAndCloudBackup\logs.txt", s + "\n");
                saveFolders += s + ";";
            }
            //TODO: Save file locations on click of Save button.
            //Save file locations
            Properties.Settings.Default["FileLocation"] = saveFolders;
            Properties.Settings.Default.Save();
        }

        internal void setBackupFolders(string backupFolder)
        {
            Properties.Settings.Default["BackupLocation"] = backupFolder;
            Properties.Settings.Default.Save();
        }
    }
}
